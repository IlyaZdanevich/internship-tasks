//
//  FormViewController.swift
//  lesson18
//
//  Created by Harbros 66 on 8.06.21.
//

import UIKit

private enum Const {
    static let step = 1
    static let regionCodeLen = 5
    static let codeLen = 7
    static let firstHyphenLen = 11
    static let secondHyphenLen = 14
    static let numberMaxLen = 17

    static let phoneMask = "+XXX(XX)XXX-XX-XX"
    static let dateMask = "XX.XX.XXXX"

    static let errorColor: CGColor = UIColor.red.cgColor
}

class FormViewController: UIViewController, UITextFieldDelegate {
    // so i met a very peculiar behaviour so i have to implement resist bool
    // it's made for textfield not to become editable again on disappear/appear of VC
    private var resist = false
    override var prefersStatusBarHidden: Bool { return true }

    private(set) var formView = FormView()
    private var formData = FormData()
    private let gestureRecog = UIGestureRecognizer()
    private var activeTextField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubviews()
        addDelegate()
        addTarget()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardNotification),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil
        )
    }

    @objc private func keyboardNotification(notification: Notification) {
        print("keyboard will change frame")
        guard
            let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let activeField = self.activeTextField,
            endFrame.origin.y < activeField.frame.maxY
                + view.safeAreaLayoutGuide.layoutFrame.origin.y
        else {
            return
        }

        formView.frame.origin.y -= activeField.frame.maxY
            - endFrame.origin.y
            + view.safeAreaLayoutGuide.layoutFrame.origin.y
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        formView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        formView.endEditing(true)
        resist = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        formView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        resist = false
    }

    private func addSubviews() {
        formView.backgroundColor = .white
        formView.readyButton.isEnabled = false
        view.addSubview(formView)
    }

    private func addDelegate() {
        for view in formView.subviews as [UIView] {
            if let textField = view as? UITextField {
                textField.delegate = self
            }
        }
    }

    private func addTarget() {
        formView.readyButton.addTarget(self, action: #selector(applyForm), for: .touchUpInside)
    }

    @objc func applyForm() {
        for view in formView.subviews as [UIView] {
            if let textField = view as? UITextField {
                guard
                    let key = textField.placeholder,
                    let value = textField.text
                else { return }
                formData.applyData(key: key, value: value)
            }
        }

        navigationController?.pushViewController(ShowFormViewController(formData), animated: true)
    }

    private func checkValidity() -> Bool {
        var check = true
        for view in formView.subviews as [UIView] {
            if let textField = view as? UITextField {
                guard
                    let text = textField.text,
                    let tag = FieldTags(rawValue: textField.tag)
                else {
                    return false
                }

                switch tag {
                case .emailTag:
                    check = mark(textField, if: text.isEmail && !text.isEmpty) && check
                case .phoneTag:
                    check = mark(textField, if: text.isRedactedPhoneNumber && !text.isEmpty) && check
                case .birthdateTag:
                    check = mark(textField, if: text.isDate && !text.isEmpty) && check
                default:
                    check = mark(textField, if: !text.isEmpty) && check
                }
            }
        }
        return check
    }

    private func mark(_ textField: UITextField, if condition: Bool) -> Bool {
        if !condition {
            textField.layer.borderWidth = 1
            textField.layer.borderColor = Const.errorColor
            return false
        }
        textField.layer.borderWidth = .zero
        return true
    }

    // text field delegate
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        if textField.tag == FieldTags.phoneTag.rawValue {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = RegexCheck.format(with: Const.phoneMask, phone: newString)
            return false
        }
        if textField.tag == FieldTags.birthdateTag.rawValue {
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = RegexCheck.format(with: Const.dateMask, phone: newString)
            return false
        }
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return !resist
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
        textField.becomeFirstResponder()
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        formView.frame = view.safeAreaLayoutGuide.layoutFrame
        self.activeTextField = nil
        formView.readyButton.isEnabled = checkValidity()
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        formView.frame = view.safeAreaLayoutGuide.layoutFrame
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) {
            textField.resignFirstResponder()
            return nextField.becomeFirstResponder()
         } else {
            return textField.resignFirstResponder()
         }
    }

    // gesture recognizer application
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        formView.endEditing(true)
    }
}
