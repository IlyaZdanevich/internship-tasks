//
//  FormElementCenterings.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import UIKit

enum ViewPosition {
    case name
    case lastname
    case birthdate
    case email
    case address
    case number

    var info: (row: CGFloat, column: CGFloat) {
        switch self {
        case .name:
            return (0, 0)
        case .lastname:
            return (1, 0)
        case .birthdate:
            return (2, 0)
        case .email:
            return (0, 1)
        case .address:
            return (1, 1)
        case .number:
            return (2, 1)
        }
    }
}

enum ViewConst {
    static let fieldMaxHeight: CGFloat = 50
    static let gapHeight: CGFloat = 10
    static let gapWidth: CGFloat = 50
    static let half: CGFloat = 0.5
    static let heightOfButtonRow: CGFloat = 50
    static let numberOfRowsPortrait: CGFloat = 6
    static var numberOfRowsLandscape: CGFloat {
        return numberOfRowsPortrait / 2
    }
}

class ViewSize {
    static func fieldRect(_ frame: CGRect) -> CGRect {
        guard frame.height < frame.width else {
            return CGRect(
                origin: .zero,
                size: CGSize(
                    width: frame.width - ViewConst.gapWidth,
                    height: min(
                        (frame.height - ViewConst.heightOfButtonRow) / ViewConst.numberOfRowsPortrait,
                        ViewConst.fieldMaxHeight
                    )
                )
            )
        }
        return CGRect(
            origin: .zero,
            size: CGSize(
                width: (frame.width - ViewConst.gapWidth) * ViewConst.half,
                height: min(
                    (frame.height - ViewConst.heightOfButtonRow) / ViewConst.numberOfRowsLandscape,
                    ViewConst.fieldMaxHeight
                )
            )
        )
    }

    static func getRectCenter(on frame: CGRect, by position: (row: CGFloat, column: CGFloat)) -> CGPoint {
        guard frame.height < frame.width else {
            return CGPoint(
                x: frame.midX,
                y: (frame.height - ViewConst.heightOfButtonRow) /
                    ViewConst.numberOfRowsPortrait * (ViewConst.half + 3 * position.column + position.row)
            )
        }
        return CGPoint(
            x: frame.midX * (ViewConst.half + position.column),
            y: (frame.height - ViewConst.heightOfButtonRow) /
                ViewConst.numberOfRowsLandscape * (ViewConst.half + position.row)
        )
    }
}
