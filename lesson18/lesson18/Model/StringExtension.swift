//
//  StringExtension.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import Foundation

private enum RegexPatterns {
    static let emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    static let numberPattern = "\\+375[0-9]{9}"
    // +XXX(XX)XXX-XX-XX
    static let redactedNumberPattern = "\\+[0-9]{3}\\([0-9]{2}\\)[0-9]{3}\\-[0-9]{2}\\-[0-9]{2}"
    static let datePattern = "[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}"
    static let digitPattern = "[0-9]+"
}

private enum LengthConst {
    static let numberLength = 13
    static let redactedNumberLength = 17
    static let dateLength = 10
}

extension String {
    var isEmail: Bool {
        return RegexCheck.stringByPattern(
            string: self,
            pattern: RegexPatterns.emailPattern
        )
    }

    var isPhoneNumber: Bool {
        return RegexCheck.stringByPattern(
            string: self,
            pattern: RegexPatterns.numberPattern,
            length: LengthConst.numberLength
        )
    }

    var isRedactedPhoneNumber: Bool {
        return RegexCheck.stringByPattern(
            string: self,
            pattern: RegexPatterns.redactedNumberPattern,
            length: LengthConst.redactedNumberLength
        )
    }

    var isDate: Bool {
        return RegexCheck.stringByPattern(
            string: self,
            pattern: RegexPatterns.datePattern,
            length: LengthConst.dateLength
        )
    }

    var isDigit: Bool {
        return RegexCheck.stringByPattern(
            string: self,
            pattern: RegexPatterns.digitPattern
        )
    }
}

class RegexCheck {
    static func stringByPattern(string: String, pattern: String, length: Int = 0) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let regexCheck = regex.firstMatch(
                in: string,
                options: NSRegularExpression.MatchingOptions(rawValue: 0),
                range: NSRange(location: 0, length: string.count)
            ) != nil

            guard length != 0 else { return regexCheck }

            let lengthCheck = string.count == length
            return regexCheck && lengthCheck
        } catch {
            return false
        }
    }

    static func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator

        // iterate over the mask characters until the iterator of numbers ends
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                // mask requires a number in this place, so take the next one
                result.append(numbers[index])

                // move numbers iterator to the next index
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }
}
