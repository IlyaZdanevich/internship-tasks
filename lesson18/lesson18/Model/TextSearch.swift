//
//  TextSearch.swift
//  lesson18
//
//  Created by Harbros 66 on 8.06.21.
//

import UIKit

private enum StringConst {
    static let inputText = """
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
        totam rem aperiam,
        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
        sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
        sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
        Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,
        nisi ut aliquid ex ea commodi consequatur?
        Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur,
        vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
        """
}

class TextSearch {
    private typealias Const = StringConst

    func highlightText(_ string: String) -> NSMutableAttributedString {
        let attributedStringText = NSMutableAttributedString(string: Const.inputText)
        findRangesOfSubstring(string).forEach {
            attributedStringText.addAttribute(
                .foregroundColor,
                value: UIColor.red,
                range: NSRange($0, in: attributedStringText.string)
            )
        }
        return attributedStringText
    }

    func removeHighlight() -> NSMutableAttributedString {
        return NSMutableAttributedString(string: Const.inputText)
    }

    private func findRangesOfSubstring(_ substring: String) -> [Range<String.Index>] {
        let attributedStringText = NSMutableAttributedString(string: Const.inputText)
        var ranges = [Range<String.Index>]()
        var startSearchIndex = attributedStringText.string.startIndex
        while startSearchIndex < attributedStringText.string.endIndex {
            guard let foundIndexRange = attributedStringText.string.range(
                of: substring,
                options: .caseInsensitive,
                range: startSearchIndex ..< attributedStringText.string.endIndex,
                locale: .none
            ) else { break }
            ranges.append(foundIndexRange)
            startSearchIndex = foundIndexRange.upperBound
        }
        return ranges
    }
}
