//
//  DataTypeEnum.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import Foundation

// also placeholders
enum PossibleKeys: String {
    case nameKey = "Name"
    case lastnameKey = "Last name"
    case birthdateKey = "Birth date"
    case emailKey = "E-Mail"
    case addressKey = "Address"
    case phoneKey = "Phone number"
}

enum FieldTags: Int {
    case nameTag
    case lastnameTag
    case birthdateTag
    case emailTag
    case addressTag
    case phoneTag
}
