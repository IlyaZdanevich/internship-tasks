//
//  Form View.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import UIKit

private enum Const {
    static let buttonWidth: CGFloat = 100
    static let buttonHeight: CGFloat = 30
    static let buttonGapFromCenterToBottomOfScreen: CGFloat = 35
}

private enum StyleConstants {
    static let nameFieldPlaceHolder = PossibleKeys.nameKey.rawValue
    static let nameFieldBorderstyle: UITextField.BorderStyle = .roundedRect

    static let lastnameFieldPlaceHolder = PossibleKeys.lastnameKey.rawValue
    static let lastnameFieldBorderstyle: UITextField.BorderStyle = .roundedRect

    static let birthdateFieldPlaceHolder = PossibleKeys.birthdateKey.rawValue
    static let birthdateFieldBorderstyle: UITextField.BorderStyle = .roundedRect

    static let emailFieldPlaceHolder = PossibleKeys.emailKey.rawValue
    static let emailFieldBorderstyle: UITextField.BorderStyle = .roundedRect

    static let addressFieldPlaceHolder = PossibleKeys.addressKey.rawValue
    static let addressFieldBorderstyle: UITextField.BorderStyle = .roundedRect

    static let phoneNumberFieldPlaceHolder = PossibleKeys.phoneKey.rawValue
    static let phoneNumberFieldBorderstyle: UITextField.BorderStyle = .roundedRect

    static let buttonTitle = "Apply"
    static let buttonColor: UIColor = .darkGray
    static let buttonBorderWidth: CGFloat = 1
    static let buttonBorderColor: CGColor = UIColor.gray.cgColor
    static let buttonBorderCornerRadius: CGFloat = 5
}

class FormView: UIView {
    private typealias Style = StyleConstants
    private(set) var nameTextField = UITextField()
    private(set) var lastnameTextField = UITextField()
    private(set) var birthdateTextField = UITextField()
    private(set) var emailTextField = UITextField()
    private(set) var addressTextField = UITextField()
    private(set) var phoneNumberTextField = UITextField()
    private(set) var readyButton = UIButton()

    init() {
        super.init(frame: .zero)
        addSubviews()
        addConstraints()
        addTag()
    }

    override func layoutSubviews() {
        for view in subviews as [UIView] {
            if let field = view as? UITextField {
                field.frame = ViewSize.fieldRect(frame)
            }
        }
        nameTextField.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.name.info)
        lastnameTextField.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.lastname.info)
        birthdateTextField.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.birthdate.info)
        emailTextField.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.email.info)
        addressTextField.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.address.info)
        phoneNumberTextField.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.number.info)

        readyButton.frame = CGRect(
            origin: .zero,
            size: CGSize(
                width: Const.buttonWidth,
                height: Const.buttonHeight
            )
        )
        readyButton.center = CGPoint(
            x: frame.midX,
            y: frame.height - Const.buttonGapFromCenterToBottomOfScreen
        )
    }

    private func addSubviews() {
        nameTextField.borderStyle = Style.nameFieldBorderstyle
        nameTextField.placeholder = Style.nameFieldPlaceHolder
        addSubview(nameTextField)

        lastnameTextField.borderStyle = Style.lastnameFieldBorderstyle
        lastnameTextField.placeholder = Style.lastnameFieldPlaceHolder
        addSubview(lastnameTextField)

        birthdateTextField.borderStyle = Style.birthdateFieldBorderstyle
        birthdateTextField.placeholder = Style.birthdateFieldPlaceHolder
        addSubview(birthdateTextField)

        emailTextField.borderStyle = Style.emailFieldBorderstyle
        emailTextField.placeholder = Style.emailFieldPlaceHolder
        addSubview(emailTextField)

        addressTextField.borderStyle = Style.addressFieldBorderstyle
        addressTextField.placeholder = Style.addressFieldPlaceHolder
        addSubview(addressTextField)

        phoneNumberTextField.borderStyle = Style.phoneNumberFieldBorderstyle
        phoneNumberTextField.placeholder = Style.phoneNumberFieldPlaceHolder
        addSubview(phoneNumberTextField)

        readyButton.setTitle(Style.buttonTitle, for: .normal)
        readyButton.setTitleColor(Style.buttonColor, for: .normal)
        readyButton.layer.cornerRadius = Style.buttonBorderCornerRadius
        readyButton.layer.borderWidth = Style.buttonBorderWidth
        readyButton.layer.borderColor = Style.buttonBorderColor
        addSubview(readyButton)
    }

    func clearForms() {
        nameTextField.text = .none
        lastnameTextField.text = .none
        birthdateTextField.text = .none
        emailTextField.text = .none
        addressTextField.text = .none
        phoneNumberTextField.text = .none
    }

    private func addConstraints() {
        nameTextField.keyboardType = .namePhonePad
        lastnameTextField.keyboardType = .namePhonePad
        birthdateTextField.keyboardType = .numbersAndPunctuation
        emailTextField.keyboardType = .emailAddress
        addressTextField.keyboardType = .default
        phoneNumberTextField.keyboardType = .numberPad
    }

    private func addTag() {
        nameTextField.tag = FieldTags.nameTag.rawValue
        lastnameTextField.tag = FieldTags.lastnameTag.rawValue
        birthdateTextField.tag = FieldTags.birthdateTag.rawValue
        emailTextField.tag = FieldTags.emailTag.rawValue
        addressTextField.tag = FieldTags.addressTag.rawValue
        phoneNumberTextField.tag = FieldTags.phoneTag.rawValue
    }

    required init?(coder: NSCoder) { super.init(coder: coder) }
}
