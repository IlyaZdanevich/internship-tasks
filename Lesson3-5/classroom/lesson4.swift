//
//  lesson4.swift
//  classroom
//
//  Created by Harbros 66 on 17.05.21.
//

import Foundation

class Lesson4 {
    func taskOne() {
        enum Fastfood {
            case burger
            case cola
            case fries
            case wrap
            case icecream
        }

        let order: [Fastfood] = [.burger, .cola]
        print("You ordered: ")
        for position in order {
            print("\(position) ")
        }
    }
    func taskTwo() {
        enum Fastfood {
            case burger(Int)
            case cola(Double)
            case fries(Int)
            case sprite(Double)
            case icecream(String)
        }

        let order: [Fastfood] = [.burger(1), .cola(0.25), .icecream("Strawberry")]
        print("You ordered: ")
        for position in order {
            print("\(position) ")
        }
    }
    func taskThree() {
        enum Fastfood {
            case burger(Int)
            case cola(Double)
            case fries(Int)
            case sprite(Double)
            case icecream(String)
            func output() {
                print(self)
            }
        }

        let order: [Fastfood] = [.burger(1), .cola(0.25), .icecream("Strawberry")]
        print("You ordered but with function: ")
        for position in order {
            position.output()
        }
    }
    func taskFour() {
        enum Fastfood {
            case burger(Int, isAvailable: Bool = .random())
            case cola(Double, isAvailable: Bool = .random())
            case fries(Int, isAvailable: Bool = .random())
            case sprite(Double, isAvailable: Bool = .random())
            case icecream(String, isAvailable: Bool = .random())
            func checkAvailability() -> Bool {
                switch self {
                case .burger(_, let isAvailable):
                    return isAvailable
                case .cola(_, let isAvailable):
                    return isAvailable
                case .fries(_, let isAvailable):
                    return isAvailable
                case .sprite(_, let isAvailable):
                    return isAvailable
                case .icecream(_, let isAvailable):
                    return isAvailable
                }
            }

            func itemName() -> (name: String, quantityString: String) {
                switch self {
                case .burger(let quantity, _):
                    return (name: "Burger", quantityString: "\(quantity)")
                case .cola(let quantity, _):
                    return (name: "Cola", quantityString: "\(quantity)l")
                case .fries(let quantity, _):
                    return (name: "Fries", quantityString: "\(quantity)")
                case .sprite(let quantity, _):
                    return (name: "Sprite", quantityString: "\(quantity)l")
                case .icecream(let quantity, _):
                    return ("Icecream", quantityString: "\(quantity) flavor")
                }
            }
        }
        let order: [Fastfood] = [.burger(1), .cola(0.25), .icecream("Strawberry")]
        print("You ordered but with availability: ")
        for position in order {
            if !position.checkAvailability() {
                print("Sorry for inconvenience, but \(position.itemName().name) is unavailable.")
            } else {
                print("\(position.itemName().name), \(position.itemName().quantityString)")
            }
        }
    }
}
