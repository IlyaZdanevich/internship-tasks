//
//  task1.swift
//  Lesson8
//
//  Created by Harbros 66 on 20.05.21.
//

import UIKit

// Task One:
extension UIColor {
    static func random() -> UIColor {
        return Self(
            red: CGFloat.random(in: 0.0...1.0),
            green: CGFloat.random(in: 0.0...1.0),
            blue: CGFloat.random(in: 0.0...1.0),
            alpha: 1
        )
    }
}

// Task Two:
extension Array where Element == Int {
    static func randomUniqueIntArray() -> [Int] {
        var uniqueSet = Set<Int>()
        (1...10).forEach { _ in
            uniqueSet.insert(.random(in: 1...1000))
        }
        return Array(uniqueSet)
    }
}
