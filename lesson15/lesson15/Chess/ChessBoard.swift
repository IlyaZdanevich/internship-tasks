//
//  ChessBoard.swift
//  lesson15
//
//  Created by Harbros 66 on 31.05.21.
//

import UIKit

private enum ChessConstants {
    static let deviceScreenSize = UIScreen.main.bounds.size

    static let halfScreenDivisionRate: CGFloat = 2
    static let oddEvenCheck = 2

    static let sideRelationToScreenSize: CGFloat = 8
    static let rowCount = 8
    static let columnCount = 8
    static let halfRowCount = rowCount / 2
    static let halfColumnCount = columnCount / 2

    static let piecesCount = 12
    static let mainBoardBlackColor = UIColor.black
    static let altBoardBlackColor = UIColor.darkGray
    static let boardWhiteColor = UIColor.white

    static let firstPieceColor = UIColor.magenta
    static let secondPieceColor = UIColor.yellow
    static let pieceRoundnessDivisionRate: CGFloat = 2
}

class ChessBoard {
    private(set) var mainView = UIView(frame: UIScreen.main.bounds)

    private typealias Const = ChessConstants

    private let squareMaxSide: CGFloat
    private let squareSize: CGSize

    private var boardView: UIView?
    private var possiblePiecesPoints = [CGPoint]()

    init() {
        squareMaxSide = min(Const.deviceScreenSize.height, Const.deviceScreenSize.width)
            / Const.sideRelationToScreenSize
        squareSize = CGSize(width: squareMaxSide, height: squareMaxSide)
        boardView = UIView(
            frame: CGRect(
                origin: getOffset(),
                size: CGSize(
                    width: min(Const.deviceScreenSize.height, Const.deviceScreenSize.width),
                    height: min(Const.deviceScreenSize.height, Const.deviceScreenSize.width)
                )
            )
        )

        guard let boardView = boardView else { return }
        mainView.addSubview(boardView)

        addSquares()
        addPieces()
    }

    private func getOffset(isLandscape: Bool = false) -> CGPoint {
        var offset = CGPoint()
        if isLandscape {
            offset.x = Const.deviceScreenSize.height / Const.halfScreenDivisionRate
                - squareMaxSide * CGFloat(Const.halfColumnCount)
        } else {
            offset.y = Const.deviceScreenSize.height / Const.halfScreenDivisionRate
                - squareMaxSide * CGFloat(Const.halfRowCount)
        }
        return offset
    }

    func rotateBoard(isLandscape: Bool) {
        guard let boardView = boardView else { return }
        var currentPossiblePoints = possiblePiecesPoints
        boardView.subviews.forEach { subview in
            switch subview.backgroundColor {
            case Const.mainBoardBlackColor :
                subview.backgroundColor = Const.altBoardBlackColor
            case Const.altBoardBlackColor :
                subview.backgroundColor = Const.mainBoardBlackColor
            case Const.firstPieceColor, Const.secondPieceColor :
                guard let position = currentPossiblePoints.randomElement() else { return }
                subview.frame.origin = position
                currentPossiblePoints.removeAll(where: { $0 == position })
            default :
                break
            }
        }

        boardView.frame.origin = getOffset(isLandscape: isLandscape)
    }

    private func addSquares() {
        guard let boardView = boardView else { return }
        (0..<Const.rowCount).forEach { row in
            (0..<Const.columnCount).forEach { column in
                let origin = CGPoint(
                    x: CGFloat(column) * squareMaxSide,
                    y: CGFloat(row) * squareMaxSide
                )
                let boardSquareView = UIView(
                    frame: CGRect(
                        origin: origin,
                        size: squareSize
                    )
                )
                boardSquareView.backgroundColor = (row + column).isMultiple(of: Const.oddEvenCheck)
                    ? Const.boardWhiteColor : Const.mainBoardBlackColor
                if boardSquareView.backgroundColor == Const.mainBoardBlackColor {
                    possiblePiecesPoints.append(origin)
                }
                boardView.addSubview(boardSquareView)
            }
        }
    }

    private func addPieces() {
        guard let boardView = boardView else { return }
        (0..<Const.piecesCount).forEach { pieceNumber in
            let firstPieceView = UIView(
                frame: CGRect(
                    origin: possiblePiecesPoints[pieceNumber],
                    size: squareSize
                )
            )
            firstPieceView.backgroundColor = Const.firstPieceColor
            firstPieceView.layer.cornerRadius = squareMaxSide / Const.pieceRoundnessDivisionRate
            
            let secondPieceView = UIView(
                frame: CGRect(
                    origin: possiblePiecesPoints[(possiblePiecesPoints.count - 1) - pieceNumber],
                    size: squareSize
                )
            )
            secondPieceView.backgroundColor = Const.secondPieceColor
            secondPieceView.layer.cornerRadius = squareMaxSide / Const.pieceRoundnessDivisionRate
            
            boardView.addSubview(firstPieceView)
            boardView.addSubview(secondPieceView)
        }
    }
}
