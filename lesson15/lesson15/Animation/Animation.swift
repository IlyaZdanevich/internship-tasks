//
//  Animation.swift
//  lesson15
//
//  Created by Harbros 66 on 31.05.21.
//
import UIKit

private enum AnimationConstants {
    static let superViewHeight = UIScreen.main.bounds.height
    static let superViewWidth = UIScreen.main.bounds.width

    static let preferedValueForSize: CGFloat = CGFloat(min(superViewHeight, superViewWidth)) / 8
    static let offset: CGFloat = (superViewHeight - preferedValueForSize * 7) / 2
    static let squareSize = CGSize(width: preferedValueForSize, height: preferedValueForSize)

    static let firstSquarePositionRelatedToMaxSize: CGFloat = preferedValueForSize * 0
    static let secondSquarePositionRelatedToMaxSize: CGFloat = preferedValueForSize * 2
    static let thirdSquarePositionRelatedToMaxSize: CGFloat = preferedValueForSize * 4
    static let forthSquarePositionRelatedToMaxSize: CGFloat = preferedValueForSize * 6

    static let startCoordXOfMovingSquare = preferedValueForSize
    static let endCoordXOfMovingSquare = preferedValueForSize * 6.5

    static let greenColor = UIColor.green
    static let redColor = UIColor.red
    static let blueColor = UIColor.blue
    static let yellowColor = UIColor.yellow

    static let topLeftCorner = CGPoint(
        x: 0,
        y: 0
    )
    static let topRightCorner = CGPoint(
        x: superViewWidth - preferedValueForSize,
        y: 0
    )
    static let botLeftCorner = CGPoint(
        x: 0,
        y: superViewHeight - preferedValueForSize
    )
    static let botRightCorner = CGPoint(
        x: superViewWidth - preferedValueForSize,
        y: superViewHeight - preferedValueForSize
    )

    static let runningLeftToRightStartingXCoord = preferedValueForSize * -1
    static let runningLeftToRightEndingXCoord = preferedValueForSize * 8.5
    static let runningRightToLeftStartingXCoord = preferedValueForSize * 9
    static let runningRightToLeftEndingXCoord = preferedValueForSize * -0.5

    static let firstRunningAnimationYCoord = preferedValueForSize
    static let secondRunningAnimationYCoord = preferedValueForSize * 3
    static let thirdRunningAnimationYCoord = preferedValueForSize * 5

    static let humanAnimationPreferredWidth = preferedValueForSize * 2 / 3
    static let humanAnimationPreferredHeight = preferedValueForSize
    static let humanRunningAnimationDuration: TimeInterval = 1

    static let numberOfFrames = 12
}

class Animation {
    private(set) var mainView = UIView(frame: UIScreen.main.bounds)

    private typealias Const = AnimationConstants

    init() {
        taskOne()
        taskTwo()
        taskThree()
    }

    // MARK: - task one
    func taskOne() {
        let squareOneView = createSquare(
            originx: Const.startCoordXOfMovingSquare,
            originy: Const.offset + Const.firstSquarePositionRelatedToMaxSize
        )
        squareOneView.backgroundColor = .random()

        let squareTwoView = createSquare(
            originx: Const.startCoordXOfMovingSquare,
            originy: Const.offset + Const.secondSquarePositionRelatedToMaxSize
        )
        squareTwoView.backgroundColor = .random()

        let squareThreeView = createSquare(
            originx: Const.startCoordXOfMovingSquare,
            originy: Const.offset + Const.thirdSquarePositionRelatedToMaxSize
        )
        squareThreeView.backgroundColor = .random()

        let squareFourView = createSquare(
            originx: Const.startCoordXOfMovingSquare,
            originy: Const.offset + Const.forthSquarePositionRelatedToMaxSize
        )
        squareFourView.backgroundColor = .random()

        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear, .autoreverse, .repeat]) {
            squareOneView.backgroundColor = .random()
            squareOneView.center.x = Const.endCoordXOfMovingSquare
        }
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn, .autoreverse, .repeat]) {
            squareTwoView.backgroundColor = .random()
            squareTwoView.center.x = Const.endCoordXOfMovingSquare
        }
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseOut, .autoreverse, .repeat]) {
            squareThreeView.backgroundColor = .random()
            squareThreeView.center.x = Const.endCoordXOfMovingSquare
        }
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseInOut, .autoreverse, .repeat]) {
            squareFourView.backgroundColor = .random()
            squareFourView.center.x = Const.endCoordXOfMovingSquare
        }

        mainView.addSubview(squareOneView)
        mainView.addSubview(squareTwoView)
        mainView.addSubview(squareThreeView)
        mainView.addSubview(squareFourView)
    }

    // MARK: - task two with corresponding methods
    func taskTwo() {
        let redView = createSquare(Const.topLeftCorner)
        redView.backgroundColor = Const.redColor

        let yellowView = createSquare(Const.topRightCorner)
        yellowView.backgroundColor = Const.yellowColor

        let greenView = createSquare(Const.botRightCorner)
        greenView.backgroundColor = Const.greenColor

        let blueView = createSquare(Const.botLeftCorner)
        blueView.backgroundColor = Const.blueColor

        mainView.addSubview(redView)
        mainView.addSubview(yellowView)
        mainView.addSubview(greenView)
        mainView.addSubview(blueView)

        randomSquareRotation(true)
    }

    private func randomSquareRotation(_: Bool) {
        if Bool.random() {
            rotateSquaresClockwise()
        } else {
            rotateSquaresAntiClockwise()
        }
    }

    private func rotateSquaresClockwise() {
        UIView.animate(
            withDuration: 3,
            delay: 0,
            options: .curveEaseInOut,
            animations: clockwiseRotationAnimation,
            completion: randomSquareRotation(_:)
        )
    }

    private func clockwiseRotationAnimation() {
        mainView.subviews.forEach { squareView in
            guard let currentBGColor = squareView.backgroundColor else { return }
            switch currentBGColor {
            case Const.redColor:
                squareView.backgroundColor = Const.yellowColor
                squareView.frame.origin = Const.topRightCorner
            case Const.yellowColor:
                squareView.backgroundColor = Const.greenColor
                squareView.frame.origin = Const.botRightCorner
            case Const.greenColor:
                squareView.backgroundColor = Const.blueColor
                squareView.frame.origin = Const.botLeftCorner
            case Const.blueColor:
                squareView.backgroundColor = Const.redColor
                squareView.frame.origin = Const.topLeftCorner
            default:
                break
            }
        }
    }

    private func rotateSquaresAntiClockwise() {
        UIView.animate(
            withDuration: 3,
            delay: 0,
            options: .curveEaseInOut,
            animations: anticlockwiseRotationAnimation,
            completion: randomSquareRotation(_:)
        )
    }

    private func anticlockwiseRotationAnimation() {
        self.mainView.subviews.forEach { squareView in
            guard let currentBGColor = squareView.backgroundColor else { return }
            switch currentBGColor {
            case Const.redColor:
                squareView.backgroundColor = Const.blueColor
                squareView.frame.origin = Const.botLeftCorner
            case Const.yellowColor:
                squareView.backgroundColor = Const.redColor
                squareView.frame.origin = Const.topLeftCorner
            case Const.greenColor:
                squareView.backgroundColor = Const.yellowColor
                squareView.frame.origin = Const.topRightCorner
            case Const.blueColor:
                squareView.backgroundColor = Const.greenColor
                squareView.frame.origin = Const.botRightCorner
            default:
                break
            }
        }
    }

    // MARK: - task three with corresponding methods
    func taskThree() {
        let humanForwardImageView = createHumanRunningAnimation(
            originx: Const.runningLeftToRightStartingXCoord,
            originy: Const.offset + Const.firstRunningAnimationYCoord
        )
        let secondHumanForwardImageView = createHumanRunningAnimation(
            originx: Const.runningLeftToRightStartingXCoord,
            originy: Const.offset + Const.thirdRunningAnimationYCoord
        )
        let humanBackwardImageView = createHumanRunningAnimation(
            originx: Const.runningRightToLeftStartingXCoord,
            originy: Const.offset + Const.secondRunningAnimationYCoord
        )
        humanBackwardImageView.transform = CGAffineTransform(scaleX: -1, y: 1)

        UIView.animate(
            withDuration: 3,
            delay: 0,
            options: [.curveEaseInOut, .repeat],
            animations: {
                humanForwardImageView.center.x = Const.runningLeftToRightEndingXCoord
                humanBackwardImageView.center.x = Const.runningRightToLeftEndingXCoord
                secondHumanForwardImageView.center.x = Const.runningLeftToRightEndingXCoord
            },
            completion: { _ in
                humanForwardImageView.stopAnimating()
                humanBackwardImageView.stopAnimating()
                secondHumanForwardImageView.stopAnimating()
            }
        )
        mainView.addSubview(humanForwardImageView)
        mainView.addSubview(secondHumanForwardImageView)
        mainView.addSubview(humanBackwardImageView)
    }

    private func createHumanRunningAnimation(originx: CGFloat, originy: CGFloat) -> UIImageView {
        let animationImageView = UIImageView(
            frame: CGRect(
                origin: CGPoint(x: originx, y: originy),
                size: CGSize(
                    width: Const.humanAnimationPreferredWidth,
                    height: Const.humanAnimationPreferredHeight
                )
            )
        )

        animationImageView.animationImages = parsePictures()
        animationImageView.animationDuration = Const.humanRunningAnimationDuration
        animationImageView.startAnimating()
        return animationImageView
    }

    private func parsePictures() -> [UIImage] {
        var forwardAnimation = [UIImage?]()
        (1...Const.numberOfFrames).forEach {
            let image = UIImage(named: "frame_\($0).png")
            forwardAnimation.append(image)
        }
        return forwardAnimation.compactMap { $0 }
    }

// MARK: - Common function for Square UIView creation
    private func createSquare(originx: CGFloat, originy: CGFloat) -> UIView {
        return createSquare(CGPoint(x: originx, y: originy))
    }

    private func createSquare(_ origin: CGPoint) -> UIView {
        return UIView(
            frame: CGRect(
                origin: origin,
                size: Const.squareSize
            )
        )
    }
}

// MARK: - RandomColor
extension UIColor {
    static func random() -> UIColor {
        return Self(
            red: CGFloat.random(in: 0.0...1.0),
            green: CGFloat.random(in: 0.0...1.0),
            blue: CGFloat.random(in: 0.0...1.0),
            alpha: 1
        )
    }
}
