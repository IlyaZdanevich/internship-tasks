//
//  CarouselItems.swift
//  Lesson21
//
//  Created by Harbros 66 on 21.06.21.
//

import UIKit

private enum Const {
    static let imageNamePrefix = "horizontal_"

    static let presetNumberOfPictures = 3
}

struct CarouselItems {
    private(set) var carouselItemPool = [UIImage]()

    init() {
        carouselItemPool = (0 ..< Const.presetNumberOfPictures).compactMap {
            UIImage(named: Const.imageNamePrefix + $0.description)
        }
    }
}
