//
//  AdItems.swift
//  Lesson21
//
//  Created by Harbros 66 on 21.06.21.
//

import UIKit

private enum Const {
    static let presetNumberOfPictures = 2

    static let imageNamePrefix = "adImage_"
}

struct AdItems {
    private(set) var adItemPool = [UIImage]()

    init() {
        adItemPool = (0 ..< Const.presetNumberOfPictures).compactMap {
            UIImage(named: Const.imageNamePrefix + $0.description)
        }
    }
}
