//
//  AdCollectionViewCell.swift
//  Lesson21
//
//  Created by Harbros 66 on 22.06.21.
//

import UIKit

private enum Const {
    static let adCellCornerRadius: CGFloat = 5
}

class AdItemImageView: UIImageView {
    init(frame: CGRect, image: UIImage) {
        super.init(frame: frame)
        self.frame = frame
        self.image = image
        self.contentMode = .scaleAspectFill

        self.layer.cornerRadius = Const.adCellCornerRadius
        self.clipsToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.frame = frame
    }

    required init?(coder: NSCoder) { super.init(coder: coder) }
}
