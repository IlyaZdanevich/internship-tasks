//
//  AdMainView.swift
//  lesson22
//
//  Created by Harbros 66 on 23.06.21.
//

import UIKit
private enum Const {
    static let half: CGFloat = 0.5
    static let isPagingEnabled = true
}

class CarouselMainScrollView: UIScrollView {
    private var carouselStackView = UIStackView()

    func fillStackView(_ carouselModel: CarouselItems) {
        let imageFrame = CGRect(
            x: .zero,
            y: .zero,
            width: bounds.width,
            height: bounds.height
        )
        carouselModel.carouselItemPool.forEach { carouselItem in
            let imageView = CarouselItemImageView(frame: imageFrame, image: carouselItem)
            carouselStackView.addArrangedSubview(imageView)
        }
        carouselStackView.distribution = .fillEqually
        self.isPagingEnabled = Const.isPagingEnabled
        addSubview(carouselStackView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        carouselStackView.frame.size = CGSize(
            width: frame.width * CGFloat(carouselStackView.arrangedSubviews.count),
            height: frame.height
        )
        self.contentSize = CGSize(
            width: frame.width * CGFloat(carouselStackView.arrangedSubviews.count),
            height: frame.height
        )
    }
}
