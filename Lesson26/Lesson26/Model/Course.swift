//
//  Course.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import RealmSwift

@objcMembers
class Course: Object, Updateable {
    dynamic var id: String = UUID().uuidString
    dynamic var name = ""
    dynamic var field = ""
    let students = List<Student>()
    dynamic var teacher: Teacher?

    func updateWithObject(newObject newCourse: Object) {
        guard let newCourse = newCourse as? Course else { return }
        name = newCourse.name
        field = newCourse.field
    }
}
