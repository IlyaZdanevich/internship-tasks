//
//  StudentTableCell.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import UIKit

private enum Const {
    static let half: CGFloat = 0.5
}

class CourseTableCell: UITableViewCell {
    let nameLabel = UILabel()
    let fieldLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.separatorInset = .zero
        addSubview(nameLabel)
        addSubview(fieldLabel)
        configureLabels()
    }

    func configure(for model: Course) {
        labelLabels(with: model)
    }

    private func labelLabels(with model: Course) {
        nameLabel.text = model.name.capitalized
        fieldLabel.text = model.field.capitalized
    }

    private func configureLabels() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            nameLabel.topAnchor.constraint(equalTo: topAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: centerYAnchor)
        ])
        nameLabel.adjustsFontSizeToFitWidth = true

        fieldLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            fieldLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            fieldLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            fieldLabel.topAnchor.constraint(equalTo: centerYAnchor),
            fieldLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        fieldLabel.adjustsFontSizeToFitWidth = true
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
