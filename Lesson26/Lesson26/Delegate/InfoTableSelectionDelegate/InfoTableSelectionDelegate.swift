//
//  InfoTableSelection.swift
//  Lesson26
//
//  Created by Harbros 66 on 10.07.21.
//

import RealmSwift

class InfoTableSelectionDelegate<T: Object>: NSObject, UITableViewDelegate {
    private weak var DBProvider: DBProviderForInfoTable?
    init(provider: DBProviderForInfoTable) {
        DBProvider = provider
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch T.self {
        case is Student.Type:
            DBProvider?.addStudent(indexPath.row)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case is Teacher.Type:
            DBProvider?.addTeacher(indexPath.row)
        default:
            return
        }
    }
}
