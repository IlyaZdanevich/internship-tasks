//
//  InfoTableDelegate.swift
//  Lesson26
//
//  Created by Harbros 66 on 9.07.21.
//

import RealmSwift

class InfoTableDelegate<T: Object>: NSObject, UITableViewDelegate {
    private weak var DBProvider: DBProviderForInfoTable?
    init(provider: DBProviderForInfoTable) {
        DBProvider = provider
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        DBProvider?.openObject(indexPath.row)
    }
}
