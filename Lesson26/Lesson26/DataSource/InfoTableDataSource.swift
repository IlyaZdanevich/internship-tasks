//
//  InfoTableDataSource.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import RealmSwift

class InfoTableDataSource<T: Object>: NSObject, UITableViewDataSource {
    weak var DBProvider: DBProviderForInfoTable?
    init(provider: DBProviderForInfoTable) {
        DBProvider = provider
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfItems = DBProvider?.getNumberOfItems() else { return .zero }
        return numberOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch T.self {
        case is Student.Type:
            let cell = tableView.dequeueReusableCell(withIdentifier: GConst.studentCellID, for: indexPath)
            return configureStudentCell(cell, at: indexPath)
        case is Teacher.Type:
            let cell = tableView.dequeueReusableCell(withIdentifier: GConst.teacherCellID, for: indexPath)
            return configureTeacherCell(cell, at: indexPath)
        case is Course.Type:
            let cell = tableView.dequeueReusableCell(withIdentifier: GConst.courceCellID, for: indexPath)
            return configureCourseCell(cell, at: indexPath)
        default:
            return UITableViewCell()
        }
    }

    func configureStudentCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard
            let studentCell = cell as? StudentTableCell,
            let studentAtIndex = DBProvider?.getItem(indexPath.row) as? Student
        else { return UITableViewCell() }
        studentCell.configure(for: studentAtIndex)
        return studentCell
    }

    private func configureTeacherCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard
            let teacherCell = cell as? TeacherTableCell,
            let teacherAtIndex = DBProvider?.getItem(indexPath.row) as? Teacher
        else { return UITableViewCell() }
        teacherCell.configure(for: teacherAtIndex)
        return teacherCell
    }

    private func configureCourseCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard
            let courceCell = cell as? CourseTableCell,
            let courseAtIndex = DBProvider?.getItem(indexPath.row) as? Course
        else { return UITableViewCell() }
        courceCell.configure(for: courseAtIndex)
        return courceCell
    }

    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }
        guard DBProvider?.deleteObject(indexPath.row) != nil else { return }
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
}
