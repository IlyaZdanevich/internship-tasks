//
//  StringExtension.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import Foundation

private enum RegexPatterns {
    static let emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    static let namePattern = "\\d"
}

extension String {
    var isEmail: Bool {
        return RegexCheck.stringByPattern(
            string: self,
            pattern: RegexPatterns.emailPattern
        )
    }
    var isName: Bool {
        return !RegexCheck.stringByPattern(
            string: self,
            pattern: RegexPatterns.namePattern
        )
    }
}

extension Optional where Wrapped == String {
    var isEmpty: Bool {
        guard let string = self else { return false }
        return string.isEmpty
    }
}

class RegexCheck {
    static func stringByPattern(string: String, pattern: String, length: Int = 0) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let regexCheck = regex.firstMatch(
                in: string,
                options: NSRegularExpression.MatchingOptions(rawValue: 0),
                range: NSRange(location: 0, length: string.count)
            ) != nil

            guard length != 0 else { return regexCheck }

            let lengthCheck = string.count == length
            return regexCheck && lengthCheck
        } catch {
            return false
        }
    }
}
