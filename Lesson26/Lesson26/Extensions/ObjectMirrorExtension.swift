//
//  AnyMirrorExtension.swift
//  Lesson26
//
//  Created by Harbros 66 on 8.07.21.
//

import RealmSwift

extension Object {
    func properties() -> [String] {
        let mirror = Mirror(reflecting: self).children.filter {
            type(of: $0.value) == String.self &&
                $0.label != "id"
        }
        return mirror.compactMap { $0.label }
    }

    static func classProperties() -> [String] {
        return self.init().properties()
    }

    static var className: String? {
        return String(describing: self)
    }
}
