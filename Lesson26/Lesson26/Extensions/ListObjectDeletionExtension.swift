//
//  ListObjectDeletionExtension.swift
//  Lesson26
//
//  Created by Harbros 66 on 10.07.21.
//

import RealmSwift

extension List {
    func delete(object: Element) {
        guard let indexToDelete = self.index(of: object) else { return }
        self.remove(at: indexToDelete)
    }
}
