//
//  DBProviderForForm.swift
//  Lesson26
//
//  Created by Harbros 66 on 12.07.21.
//

import RealmSwift

protocol DBProviderForFormTable: AnyObject {
    func openObject(_ atIndexPath: IndexPath)
    func addObject(_ type: Int)
    func checkTeacher() -> Bool
    func deleteTeacher() -> ()
    func deleteStudent(_ atIndex: IndexPath)
}
