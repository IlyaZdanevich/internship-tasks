//
//  FormTableViewController.swift
//  Lesson26
//
//  Created by Harbros 66 on 8.07.21.
//

import RealmSwift

class FormTableController<T: Object>: UIViewController {
    private let tableView = UITableView(frame: .zero, style: .insetGrouped)
    private var tableViewDataSource: FormTableDataSource<T>?
    private let tableViewSectionManager: FormTableSectionManager<T>
    private var tableViewDelegate: FormTableDelegate<T>?
    private var DBManager: DBManager<T> = .init()
    var model: T?
    
    private let saveItemButton = UIBarButtonItem(systemItem: .save)

    init(for model: T?) {
        self.model = model
        tableViewSectionManager = .init(model: model)
        super.init(nibName: nil, bundle: nil)
        tableViewDelegate = .init(provider: self)
        tableViewDataSource = .init(sectionManager: tableViewSectionManager, provider: self)
        
        tableView.dataSource = tableViewDataSource
        tableView.delegate = tableViewDelegate
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        addTableViewConstraints()
        prepareTableView()
        prepareNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    private func prepareTableView() {
        tableView.register(InputTableCell.self, forCellReuseIdentifier: GConst.inputCellID)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: GConst.addCellID)
        tableView.register(StudentTableCell.self, forCellReuseIdentifier: GConst.studentCellID)
        tableView.register(TeacherTableCell.self, forCellReuseIdentifier: GConst.teacherCellID)
        tableView.register(CourseTableCell.self, forCellReuseIdentifier: GConst.courcesCellID)

        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .purple
        tableView.keyboardDismissMode = .onDrag
    }

    private func prepareNavigationBar() {
        navigationItem.title = String(describing: T.self)
        navigationItem.setRightBarButton(saveItemButton, animated: true)
        saveItemButton.action = #selector(saveObject)
        saveItemButton.target = self
    }

    @objc private func saveObject() {
        var inputCells = [InputTableCell]()
        (0 ..< tableViewSectionManager.rowCount(for: .zero)).forEach {
            let indexPath = IndexPath(row: $0, section: .zero)
            guard let cell = tableView.cellForRow(at: indexPath) as? InputTableCell else { return }
            inputCells.append(cell)
        }
        guard let newObject = tryCreateObject(fields: inputCells) else { return }
        DBManager.addObjectByDictionary(oldObject: model, newObject: newObject)
        navigationController?.popViewController(animated: true)
    }

    private func addTableViewConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }

    required init?(coder: NSCoder) { fatalError() }
}

extension FormTableController: DBProviderForFormTable {
    func tryCreateObject(fields: [InputTableCell]) -> Object? {
        let failedFields = fields.filter { !$0.checkCorrect() }
        if !failedFields.isEmpty {
            let fieldErrorMessage = failedFields.map { field -> String in
                guard let value = field.textFieldValue else { return GConst.emptyMark }
                let errorValue = value.isEmpty ? GConst.emptyMark : value
                return "\(field.type?.rawValue ?? GConst.errorTitle): \(errorValue)"
            }

            let flatFieldError = fieldErrorMessage.reduce(String(), { $0 + "\r\n" + $1 })
            let errorMessage = """
                \(GConst.errorMessage)
                \(flatFieldError)
                """
            let alertController = UIAlertController(title: GConst.errorTitle, message: errorMessage, preferredStyle: .alert)
            let okButton = UIAlertAction(title: GConst.okTitle, style: .default)
            alertController.addAction(okButton)
            present(alertController, animated: true)
            return nil
        } else {
            guard T.classProperties().count == fields.count else { return nil }
            let newObject = T()
            fields.forEach {
                guard let key = $0.type?.rawValue else { return }
                newObject.setValue($0.textFieldValue, forKey: key)
            }
            return newObject
        }
    }

    func openObject(_ atIndexPath: IndexPath) {
        if atIndexPath == GConst.courseFormTeacherIndexPath {
            guard let teacher = (model as? Course)?.teacher else { return }
            let vc = FormTableController<Teacher>(for: teacher)
            navigationController?.pushViewController(vc, animated: true)
        } else {
            guard let student = (model as? Course)?.students.safe(at: atIndexPath.row - 1) else { return }
            let vc = FormTableController<Student>(for: student)
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    func addObject(_ type: Int) {
        guard let model = model as? Course else { return }
        switch type{
        case 1:
            let vc = InfoTableController<Teacher>(courseModel: model)
            navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = InfoTableController<Student>(courseModel: model)
            navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
    }

    func checkTeacher() -> Bool {
        guard let course = model as? Course else { return false }
        return course.teacher != nil
    }

    func deleteTeacher() {
        guard let course = model as? Course else { return }
        DBManager.tryDeleteTeacherFromCourse(course: course)
        tableView.reloadRows(at: [GConst.courseFormTeacherIndexPath], with: .automatic)
    }

    func deleteStudent(_ atIndex: IndexPath) {
        guard
            let course = model as? Course,
            let student = course.students.safe(at: atIndex.row - 1)
        else { return }
        DBManager.tryDeleteStudentFromCourse(student: student, course: course)
        tableView.deleteRows(at: [atIndex], with: .automatic)
    }
}
