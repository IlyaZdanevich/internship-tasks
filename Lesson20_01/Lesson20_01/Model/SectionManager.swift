//
//  SectionManager.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 17.06.21.
//

import UIKit

protocol TableSection {
    var sectionName: String { get }
    var sectionContent: [CellType] { get }
}

enum CellType {
    case color(Color)
    case student(Student)
}

class TableSectionManager {
    var tableSections = [TableSection]()
    private let studentGroupWorkings = StudentGroup()

    init() {
        tableSections.append(contentsOf: studentGroupWorkings.randomMarkedGroup())
        tableSections.append(ColorSection())
    }
}
