//
//  ColorSection.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 17.06.21.
//

import UIKit
enum ColorHeaderName {
    static let colorSectionName = "Colors"
}

private enum Const {
    static let colorNumber = 10
}

class ColorSection: TableSection {
    var sectionName: String
    var sectionContent: [CellType]

    init() {
        sectionName = ColorHeaderName.colorSectionName
        sectionContent = Self.generateColorArray()
    }

    static private func generateColorArray() -> [CellType] {
        var colorArray = [CellType]()
        (1...Const.colorNumber).forEach { _ in
            colorArray.append(.color(Color()))
        }
        return colorArray
    }
}
