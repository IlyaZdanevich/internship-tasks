//
//  Mark.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 17.06.21.
//

import UIKit

private enum Const {
    static let markRange = (2...5)
}

enum Mark: Int, CaseIterable {
    case excellent = 5
    case good = 4
    case average = 3
    case bad = 2

    var color: UIColor {
        switch self {
        case .excellent:
            return .green
        case .good:
            return .yellow
        case .average:
            return .orange
        case .bad:
            return .red
        }
    }

    var description: String {
        switch self {
        case .excellent:
            return "Excellent"
        case .good:
            return "Good"
        case .average:
            return "Average"
        case .bad:
            return "Bad"
        }
    }
}
