//
//  Student.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 12.06.21.
//

import UIKit

private enum Const {
    static let names = [
        "james", "robert", "john", "michael", "william", "david", "richard", "joseph", "thomas", "charles",
        "mary", "patricia", "jennifer", "linda", "elizabeth", "barbara", "susan", "jessica", "sarah", "karen"
    ]
    static let lastnames = [
        "smith", "johnson", "williams", "brown", "jones", "miller", "davis", "garcia", "rodriguez", "wilson",
        "martinez", "anderson", "taylor", "thomas", "hernandez", "moore", "martin", "jackson", "thompson", "white"
    ]
}

class Student {
    private(set) var midMark = Mark.allCases.randomElement() ?? Mark.excellent
    let textLabelText = (Const.names.randomElement()?.capitalized ?? "Ivan") + " "
        + (Const.lastnames.randomElement()?.capitalized ?? "Ivanov")

    var textLabelColor: UIColor? {
        midMark.color
    }
    var detailTextLabelText: String? {
        midMark.description
    }
    var backgroundColor: UIColor?
}
