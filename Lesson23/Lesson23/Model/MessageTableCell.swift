//
//  MessageTableCell.swift
//  Lesson23
//
//  Created by Harbros 66 on 29.06.21.
//

import UIKit

class MessageTableCell: UITableViewCell {
    private var messageView = MessageView()

    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.constraints.forEach {
            contentView.removeConstraint($0)
        }
        contentView.subviews.forEach {
            $0.removeFromSuperview()
        }
    }

    func setupCell(with message: MessageView) {
        messageView = message
        let messageFrame = messageView.frame
        contentView.addSubview(messageView)

        let pinAttribute: NSLayoutConstraint.Attribute = message.isIncoming ? .left : .right
        let pinConstraint = NSLayoutConstraint(
            item: messageView,
            attribute: pinAttribute,
            relatedBy: .equal,
            toItem: contentView,
            attribute: pinAttribute,
            multiplier: 1.0,
            constant: .zero
        )
        messageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addConstraint(pinConstraint)
        contentView.addConstraint(messageView.widthAnchor.constraint(equalToConstant: messageFrame.width))
        contentView.addConstraint(messageView.heightAnchor.constraint(equalToConstant: messageFrame.height))
    }
}
