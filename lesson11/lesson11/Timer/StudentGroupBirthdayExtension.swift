//
//  StudentGroupBirthdayExtension.swift
//  lesson11
//
//  Created by Harbros 66 on 28.05.21.
//

import Foundation

extension StudentGroup {
    func whosBirthdayIsToday(dateToCheck: Date) {
        studentGroup.forEach { student in
            if student == dateToCheck {
                student.congratulateStudent()
            }
        }
    }
}

extension Student {
    func congratulateStudent() {
        print("Happy birth day \(firstName)")
    }
}
