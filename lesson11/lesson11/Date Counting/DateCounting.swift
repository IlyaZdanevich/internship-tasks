//
//  DateCounting.swift
//  lesson11
//
//  Created by Harbros 66 on 28.05.21.
//

import Foundation

class DateCounting {
    let currentYearInComponents = DateComponents(
        calendar: .current,
        timeZone: TimeZone(secondsFromGMT: 0),
        year: 2021,
        month: 1,
        day: 1
    )
    let firstDayDate: Date
    let lastDayDate: Date

    init() {
        firstDayDate = Calendar.current.date(from: currentYearInComponents) ?? Date()
        lastDayDate = Calendar.current.date(byAdding: .year, value: 1, to: firstDayDate) ?? Date()
    }

    func task() {
        taskOne()
        taskTwo()
        taskThree()
    }

    func taskOne() {
        var date = firstDayDate
        while date < lastDayDate {
            let weekday = Calendar.current.component(.weekday, from: date)
            print(DateFormatter().weekdaySymbols[weekday - 1])
            date = Calendar.current.date(byAdding: .month, value: 1, to: date) ?? Date()
        }
    }

    func taskTwo() {
        var date = Calendar.current.date(byAdding: .day, value: 2, to: firstDayDate) ?? Date()
        while date < lastDayDate {
            print(DateTrickery.printDate(date: date))
            date = Calendar.current.date(byAdding: .day, value: 7, to: date) ?? Date()
        }
    }

    func taskThree() {
        var date = firstDayDate
        var count = 0
        var totalCount = 0
        var currentMonth = Calendar.current.component(.month, from: date)
        while date < lastDayDate {
            if
                Calendar.current.component(.month, from: date) > currentMonth
            {
                print("There are \(count) work days in \(Calendar.current.monthSymbols[currentMonth - 1])")
                count = 0
                currentMonth = Calendar.current.component(.month, from: date)
            }
            if
                (2...6).contains(Calendar.current.component(.weekday, from: date))
            {
                count += 1
                totalCount += 1
            }
            date = DateTrickery.getNextDay(date: date)
        }
        print("There are \(count) work days in \(Calendar.current.monthSymbols[currentMonth - 1])")
    }
}
