//
//  Date Tasks.swift
//  lesson11
//
//  Created by Harbros 66 on 27.05.21.
//

import Foundation

class Lesson11DateTask {
    let studentGroup = StudentGroup()
    func taskOne() {
        studentGroup.fillGroupWithNStudents(number: 50)

        studentGroup.sort(byOp: <<)

        let oldestStudent = studentGroup.max(byOp: >)
        studentGroup.printGroupPersonalInfo()
        print("Oldest student:")
        oldestStudent?.printPersonalInfo()
    }
}
