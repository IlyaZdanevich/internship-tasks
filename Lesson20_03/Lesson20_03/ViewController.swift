//
//  ViewController.swift
//  Lesson20_03
//
//  Created by Harbros 66 on 12.06.21.
//
private enum Const {
    static let cellIdentifier = "id"

    static let tableViewHeightOfScreen: CGFloat = 8 / 9
    static let buttonRowHeightOfScreen: CGFloat = 1 - tableViewHeightOfScreen
    static let buttonCount: CGFloat = 2

    static let openFolderButtonPos = 0
    static let createFolderButtonPos = 1

    static let dirTableViewBackgroundColor: UIColor = .white
    static let openFolderButtonBackgroundColor: UIColor = .green
    static let createFolderButtonBackgroundColor: UIColor = .orange

    static let openFolderButtonText = "Open folder"
    static let createFolderButtonText = "Create folder"
}

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    private let fileSystem: FileSystem
    private var selectedItemIndexPath: IndexPath?
    private let dirTableView = UITableView()
    private let openFolderButton = UIButton()
    private let createFolderButton = UIButton()

    init(path: String) {
        fileSystem = FileSystem(path)
        super.init(nibName: nil, bundle: nil)
        addDataSourceAndDelegate()
        addTargets()
        addSubviews()
        fileSystem.getSizes(completion: updateTable)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
    }

    private func addDataSourceAndDelegate() {
        dirTableView.dataSource = self
        dirTableView.delegate = self
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let frame = UIScreen.main.bounds
        dirTableView.frame = tableViewRect(frame)
        openFolderButton.frame = buttonRect(frame, pos: Const.openFolderButtonPos)
        createFolderButton.frame = buttonRect(frame, pos: Const.createFolderButtonPos)
    }

    private func addSubviews() {
        dirTableView.backgroundColor = Const.dirTableViewBackgroundColor
        openFolderButton.backgroundColor = Const.openFolderButtonBackgroundColor
        createFolderButton.backgroundColor = Const.createFolderButtonBackgroundColor

        openFolderButton.setTitle(Const.openFolderButtonText, for: .normal)
        createFolderButton.setTitle(Const.createFolderButtonText, for: .normal)

        view.addSubview(dirTableView)
        view.addSubview(openFolderButton)
        view.addSubview(createFolderButton)
    }

    private func addTargets() {
        openFolderButton.addTarget(self, action: #selector(openFolder), for: .touchUpInside)
        createFolderButton.addTarget(self, action: #selector(createFolder), for: .touchUpInside)
    }

    @objc func openFolder() {
        guard
            let selectedItemIndex = selectedItemIndexPath,
            let newDir = fileSystem.isSelectedFileFolder(selectedItemIndex)
        else {
            return
        }
        navigationController?.pushViewController(ViewController(path: newDir), animated: true)
    }

    @objc func createFolder() {
        fileSystem.createFolder()

        dirTableView.beginUpdates()
        dirTableView.insertRows(at: [IndexPath(row: .zero, section: .zero)], with: .fade)
        dirTableView.endUpdates()
    }

    func updateTable() {
        dirTableView.reloadData()
    }

    // MARK: - data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileSystem.fileDirectory.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: Const.cellIdentifier)

        let item = fileSystem.fileDirectory[indexPath.row]

        cell.textLabel?.text = item.name
        cell.textLabel?.textColor = item.type.color
        cell.detailTextLabel?.text = item.fileSize?.description

        return cell
    }

    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }
        fileSystem.removeItem(at: indexPath.row)
        dirTableView.reloadData()
    }

    // MARK: - delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItemIndexPath = indexPath
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectedItemIndexPath = nil
    }

    // MARK: - required init
    required init?(coder: NSCoder) {
        fileSystem = FileSystem("/")
        super.init(coder: coder)
    }

    // MARK: - rects
    func tableViewRect(_ frame: CGRect) -> CGRect {
        return CGRect(
            origin: .zero,
            size: CGSize(
                width: frame.width,
                height: frame.height * Const.tableViewHeightOfScreen
            )
        )
    }

    func buttonRect(_ frame: CGRect, pos: Int = 0) -> CGRect {
        return CGRect(
            x: frame.width / Const.buttonCount * CGFloat(pos),
            y: frame.height * Const.tableViewHeightOfScreen,
            width: frame.width / Const.buttonCount,
            height: frame.height * Const.buttonRowHeightOfScreen
        )
    }
}
