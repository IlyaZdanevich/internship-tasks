//
//  FileSystem.swift
//  Lesson20_03
//
//  Created by Harbros 66 on 18.06.21.
//

import Foundation

private enum Const {
    static let standartFolderName = "folder"
    static let baneOfSize = ".DS_Store"

    static let folderInsertionIndex = 0
    static let folderDigTime: TimeInterval = 10
}

class FileSystem {
    private var path = ""
    private(set) var fileDirectory = [File]()

    init(_ path: String) {
        self.path = path
        getContent()
    }

    private func getContent() {
        let contentList: [String]
        do {
            contentList = try FileManager.default.contentsOfDirectory(atPath: path)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        contentList.forEach { item in
            let itemPath = path + "/" + item
            var isDir: ObjCBool = false
            if
                FileManager.default.fileExists(atPath: itemPath, isDirectory: &isDir),
                item.first != "."
            {
                let type = isDir.boolValue ? Filetype.directory : Filetype.file
                fileDirectory.append(File(type: type, name: item, fulldir: itemPath))
            } else {
                print(itemPath)
                print("Error while processing content of directory")
            }
        }
        fileDirectory.sort { $0.name < $1.name }
    }

    func isSelectedFileFolder(_ index: IndexPath) -> String? {
            guard
                fileDirectory.indices.contains(index.row),
                fileDirectory[index.row].type != Filetype.file
            else {
                print("Nothing or File is selected")
                return nil
            }
        return fileDirectory[index.row].fulldir
    }

    func createFolder() {
        print("Create folder: ")
        var newName: String
        var counter = 1
        repeat {
            newName = Const.standartFolderName + String(counter)
            counter += 1
        } while fileDirectory.contains {
            $0.name == newName
        }
        let newFolderPath = path + "/" + newName
        do {
            try FileManager.default.createDirectory(
                atPath: newFolderPath,
                withIntermediateDirectories: true,
                attributes: .none
            )
        } catch let error {
            print(error.localizedDescription)
            return
        }

        let newFolder = File(type: .directory, name: newName, fulldir: newFolderPath)
        newFolder.fileSize = 0
        fileDirectory.insert(
            newFolder,
            at: Const.folderInsertionIndex
        )
    }

    func getSizes(completion: @escaping () -> ()) {
        let localDG = DispatchGroup()
        let localDQ = DispatchQueue.global(qos: .utility)
        fileDirectory.forEach { file in
            var dispatchItem: DispatchWorkItem? = .init { [weak self] in
                file.fileSize = self?.getsizeOfItem(item: file.fulldir, workItem: dispatchItem)
                dispatchItem = nil
            }
            guard let dispatchWork = dispatchItem else { return }
            localDQ.async(group: localDG, execute: dispatchWork)
            localDQ.asyncAfter(deadline: .now() + Const.folderDigTime) {
                dispatchItem?.cancel()
                dispatchItem = nil
            }
        }
        localDG.notify(queue: .main, execute: completion)
    }

    private func getsizeOfItem(item: String, workItem: DispatchWorkItem?) -> Int {
        var isDir: ObjCBool = false
        if workItem?.isCancelled ?? true { return .zero }
        do {
            if
                FileManager.default.fileExists(atPath: item, isDirectory: &isDir)
            {
                if isDir.boolValue {
                    var size = 0
                    let contents = try FileManager.default.contentsOfDirectory(atPath: item)
                    contents.forEach { subItem in
                        let itemName = item + "/" + subItem
                        size += getsizeOfItem(item: itemName, workItem: workItem)
                    }
                    return size
                } else {
                    guard !item.contains(Const.baneOfSize) else { return .zero }
                    let attr = try FileManager.default.attributesOfItem(atPath: item)
                    let _size = attr[.size] as? NSNumber
                    guard let size = _size else { return .zero }
                    return size.intValue
                }
            } else {
                return .zero
            }
        } catch let error {
            print(error.localizedDescription)
            return .zero
        }
    }

    func removeItem(at index: Int) {
        let item = fileDirectory[index]
        do {
            try FileManager.default.removeItem(atPath: item.fulldir)
            fileDirectory.remove(at: index)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
