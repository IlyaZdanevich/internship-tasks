//
//  Group.swift
//  Lesson20_04
//
//  Created by Harbros 66 on 19.06.21.
//

import Foundation

struct StudentGroup {
    let group: [Student]
    var numberOfRows: Int {
        group.count
    }

    init(group: [Student]) {
        self.group = group
    }
}
