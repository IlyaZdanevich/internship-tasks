//
//  SimpleGuessing.swift
//  lesson12
//
//  Created by Harbros 66 on 2.06.21.
//

import Foundation

class SimpleClass {
    private var studentGroup = [Student]()
    private let range = (1...Int.random(in: (1...1000)))
    private let localDispatchQueue = DispatchQueue(
        label: "Simple Dispatch Queue",
        qos: .default,
        attributes: .concurrent
    )
    private let localDispatchGroup = DispatchGroup()

    func task() {
        (1...5).forEach { _ in
            studentGroup.append(Student(answer: .random(in: range), range: range))
        }

        studentGroup.forEach { student in
            localDispatchQueue.async(group: localDispatchGroup) {
                student.guessTheNumber { fullname, numberOfTries in
                    print("Student \(fullname) guessed number in \(numberOfTries) times.")
                }
            }
        }

        localDispatchGroup.notify(queue: .main) {
            print("Group guessed the number")
        }
    }
}
