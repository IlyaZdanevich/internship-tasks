//
//  AppleMapViewController.swift
//  Lesson24
//
//  Created by Harbros 66 on 29.06.21.
//

import MapKit

private enum Const {
    static let regionRadiusInMeters: CLLocationDistance = 2000
    static let pinRegionMinimumSize: CGFloat = 40
    static var zoomEdgeOffsets: UIEdgeInsets {
        UIEdgeInsets(
            top: pinRegionMinimumSize,
            left: pinRegionMinimumSize,
            bottom: pinRegionMinimumSize,
            right: pinRegionMinimumSize
        )
    }

    static let annotationViewID = "markerID"
    static let attendanceTableCellID = "attendanceID"
    static let addressGetError = "No address found"

    static let half: Double = 0.5

    static let firstCircleRadius: CLLocationDistance = 1000
    static let secondCircleRadius: CLLocationDistance = 1500
    static let thirdCircleRadius: CLLocationDistance = 3000

    static let circleFillColor: UIColor = .systemBlue
    static let firstCircleFillAlpha: CGFloat = 0.2
    static let secondCircleFillAlpha: CGFloat = 0.10
    static let thirdCircleFillAlpha: CGFloat = 0.05

    static let tableViewOpacity: CGFloat = 0.4
    static let tableViewOffsetFromBorders: CGFloat = 15
    static let tableViewWidthOfView: CGFloat = 0.5

    static let numberOfRowsInAttendanceTable = 4

    static let tableFontSize: CGFloat = 14
    static let tableRowAddToFont: CGFloat = 4
    static var tableRowHeight: CGFloat {
        tableFontSize + tableRowAddToFont
    }

    static let pathStrokeColor: UIColor = .blue.withAlphaComponent(0.8)
    static let pathStrokeWidth: CGFloat = 3
}

class AppleMapViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { true }
    private let studentModel: StudentGroup
    private let meetupPlace = MeetupPlace()
    private let meetupInfo = MeetupInfo()

    private let mapView = MKMapView()
    private let geocoder = CLGeocoder()
    private let zoomButton = UIBarButtonItem(systemItem: .search)
    private let pathFindingButton = UIBarButtonItem(systemItem: .compose)

    private let attendanceTableView = UITableView()

    init(studentModel: StudentGroup) {
        self.studentModel = studentModel
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        setupButton()

        mapView.delegate = self
        mapView.addAnnotations(studentModel.studentGroup)
        mapView.addAnnotation(meetupPlace)
        drawCircles(centerOfAnnotation: meetupPlace.coordinate)
        view.addSubview(mapView)

        meetupInfo.fill(meetup: meetupPlace, studentGroup: studentModel)
        setupTable()
        view.addSubview(attendanceTableView)
        addAttendanceTableViewConstraints()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mapView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        actionZoomToFit()
    }

    private func setupButton() {
        zoomButton.target = self
        zoomButton.action = #selector(actionZoomToFit)
        pathFindingButton.target = self
        pathFindingButton.action = #selector(pathFinding)

        navigationItem.setRightBarButtonItems([zoomButton, pathFindingButton], animated: false)
    }

    @objc private func actionZoomToFit() {
        var zoomRect: MKMapRect = .null
        studentModel.studentGroup.forEach { student in
            let studentLocationRect = MKMapRect(
                origin: MKMapPoint(student.coordinate),
                size: .init()
            )
            zoomRect = zoomRect.union(studentLocationRect)
        }
        zoomRect = mapView.mapRectThatFits(zoomRect)
        mapView.setVisibleMapRect(zoomRect, edgePadding: Const.zoomEdgeOffsets, animated: true)
    }

    @objc private func pathFinding() {
        deleteAllPaths()
        studentModel.studentGroup.forEach { student in
            guard meetupInfo.checkAvailability(student: student) else { return }
            addPath(for: student)
        }
    }

    private func addPath(for student: Student) {
        let startpoint = MKPlacemark(coordinate: student.coordinate)
        let endpoint = MKPlacemark(coordinate: meetupPlace.coordinate)

        let pathRequest = MKDirections.Request()
        pathRequest.source = MKMapItem(placemark: startpoint)
        pathRequest.destination = MKMapItem(placemark: endpoint)
        pathRequest.transportType = .walking

        let direction = MKDirections(request: pathRequest)
        direction.calculate { response, _ in
            guard let response = response else { return }
            let routes = response.routes.compactMap { $0.polyline }
            self.mapView.addOverlays(routes)
        }
    }

    private func deleteAllPaths() {
        mapView.overlays.forEach { overlay in
            guard let polyline = overlay as? MKPolyline else { return }
            mapView.removeOverlay(polyline)
        }
    }

    private func drawCircles(centerOfAnnotation: CLLocationCoordinate2D) {
        let firstCircle = MKCircle(center: centerOfAnnotation, radius: Const.firstCircleRadius)
        let secondCircle = MKCircle(center: centerOfAnnotation, radius: Const.secondCircleRadius)
        let thirdCircle = MKCircle(center: centerOfAnnotation, radius: Const.thirdCircleRadius)
        mapView.addOverlays([
            firstCircle, secondCircle, thirdCircle
        ])
    }

    private func addAttendanceTableViewConstraints() {
        attendanceTableView.translatesAutoresizingMaskIntoConstraints = false
        let tableViewHeight = Const.tableRowHeight * CGFloat(Const.numberOfRowsInAttendanceTable)
        NSLayoutConstraint.activate([
            attendanceTableView.trailingAnchor.constraint(
                equalTo: view.trailingAnchor,
                constant: -Const.tableViewOffsetFromBorders
            ),
            attendanceTableView.bottomAnchor.constraint(
                equalTo: view.bottomAnchor,
                constant: -Const.tableViewOffsetFromBorders
            ),
            attendanceTableView.widthAnchor.constraint(
                equalTo: view.widthAnchor,
                multiplier: Const.tableViewWidthOfView
            ),
            attendanceTableView.heightAnchor.constraint(
                equalToConstant: tableViewHeight
            )
        ])
    }

    private func setupTable() {
        attendanceTableView.layoutMargins = .zero
        attendanceTableView.backgroundColor = .clear
        attendanceTableView.register(AttendanceTableCell.self, forCellReuseIdentifier: Const.attendanceTableCellID)
        attendanceTableView.isScrollEnabled = false
        attendanceTableView.allowsSelection = false
        attendanceTableView.delegate = self
        attendanceTableView.dataSource = self
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

// MARK: - MKMapViewDelegate
extension AppleMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var viewMarker: MKMarkerAnnotationView
        let optAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: Const.annotationViewID)
        guard let view = optAnnotationView as? MKMarkerAnnotationView else {
            viewMarker = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: Const.annotationViewID)
            if let studentAnnotation = annotation as? Student {
                viewMarker.canShowCallout = true
                viewMarker.calloutOffset = .zero
                let calloutButton = UIButton(type: .detailDisclosure)
                viewMarker.rightCalloutAccessoryView = calloutButton
                viewMarker.glyphImage = studentAnnotation.image
            }
            if let meetupAnnotation = annotation as? MeetupPlace {
                viewMarker.glyphImage = meetupAnnotation.image
                viewMarker.isDraggable = true
            }
            return viewMarker
        }
        view.annotation = annotation
        if let studentAnnotation = annotation as? Student {
            view.glyphImage = studentAnnotation.image
        }
        if let meetupAnnotation = annotation as? MeetupPlace {
            view.glyphImage = meetupAnnotation.image
        }
        viewMarker = view
        return view
    }

    func mapView(
        _ mapView: MKMapView,
        annotationView view: MKAnnotationView,
        calloutAccessoryControlTapped control: UIControl
    ) {
        guard let pinAnnotation = view.annotation as? Student else { return }
        let userLocation = CLLocation(
            latitude: pinAnnotation.coordinate.latitude,
            longitude: pinAnnotation.coordinate.longitude
        )
        geocoder.reverseGeocodeLocation(userLocation) { placemarks, error in
            guard
                error == nil,
                let closestPlacemark = placemarks?.first
            else {
                print(Const.addressGetError)
                return
            }
            let pinStudent = StudentWithAddress(placemark: closestPlacemark, student: pinAnnotation)

            let popVC = PopoverTableViewController(student: pinStudent)
            popVC.modalPresentationStyle = .popover
            let popoverVC = popVC.popoverPresentationController
            popoverVC?.delegate = self
            popoverVC?.sourceView = view
            popoverVC?.sourceRect = CGRect(
                x: view.bounds.midX,
                y: view.bounds.minY,
                width: .zero,
                height: .zero
            )

            self.present(popVC, animated: true, completion: nil)
        }
    }

    func mapView(
        _ mapView: MKMapView,
        annotationView view: MKAnnotationView,
        didChange newState: MKAnnotationView.DragState,
        fromOldState oldState: MKAnnotationView.DragState
    ) {
        switch newState {
        case .starting:
            view.dragState = .dragging
            mapView.removeOverlays(mapView.overlays)
        case .ending, .canceling:
            view.dragState = .none
            guard let centerOfAnnotation = view.annotation?.coordinate else { return }
            drawCircles(centerOfAnnotation: centerOfAnnotation)
            meetupInfo.fill(meetup: meetupPlace, studentGroup: studentModel)
            attendanceTableView.reloadData()
        default:
            break
        }
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: circleOverlay)
            circleRenderer.strokeColor = .clear
            switch circleOverlay.radius {
            case Const.firstCircleRadius:
                circleRenderer.fillColor = Const.circleFillColor.withAlphaComponent(Const.firstCircleFillAlpha)
            case Const.secondCircleRadius:
                circleRenderer.fillColor = Const.circleFillColor.withAlphaComponent(Const.secondCircleFillAlpha)
            case Const.thirdCircleRadius:
                circleRenderer.fillColor = Const.circleFillColor.withAlphaComponent(Const.thirdCircleFillAlpha)
            default:
                break
            }
            return circleRenderer
        }
        if let routeOverlay = overlay as? MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: routeOverlay)
            polylineRenderer.strokeColor = Const.pathStrokeColor
            polylineRenderer.lineWidth = Const.pathStrokeWidth

            return polylineRenderer
        }
        return MKOverlayRenderer()
    }
}

extension AppleMapViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension AppleMapViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Const.tableRowHeight
    }
}

extension AppleMapViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Const.numberOfRowsInAttendanceTable
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Const.attendanceTableCellID, for: indexPath)
        guard let attendanceCell = cell as? AttendanceTableCell else { return cell }
        switch indexPath.item {
        case 0:
            attendanceCell.setupCell(
                lower: .zero,
                upper: Const.firstCircleRadius,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        case 1:
            attendanceCell.setupCell(
                lower: Const.firstCircleRadius,
                upper: Const.secondCircleRadius,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        case 2:
            attendanceCell.setupCell(
                lower: Const.secondCircleRadius,
                upper: Const.thirdCircleRadius,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        case 3:
            attendanceCell.setupCell(
                lower: Const.thirdCircleRadius,
                upper: .infinity,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        default:
            break
        }

        cell.layoutMargins = .zero
        cell.textLabel?.font = cell.textLabel?.font.withSize(Const.tableFontSize)
        cell.backgroundColor = .white.withAlphaComponent(Const.tableViewOpacity)
        return cell
    }
}
