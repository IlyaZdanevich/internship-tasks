//
//  ViewController.swift
//  Lesson24
//
//  Created by Harbros 66 on 29.06.21.
//

import UIKit

private enum Const {
    static let buttonViewHeightOfFrame: CGFloat = 0.3
    static let buttonViewWidthOfFrame: CGFloat = 0.7

    static let shuffleButtonOffsetFromBorders: CGFloat = 10
    static let scrableButtonWidth: CGFloat = 100
}

class MainViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { true }
    private let buttonView = ButtonView()
    private let shuffleButton = UIBarButtonItem(systemItem: .refresh)
    private var studentModel = StudentGroup()
    private var appleMapVC: AppleMapViewController?
    private var googleMapVC: GoogleMapViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(buttonView)
        navigationItem.setRightBarButton(shuffleButton, animated: true)
        addConstraints()
        addTargetsForButtons()
    }

    private func addConstraints() {
        buttonView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            buttonView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            buttonView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            buttonView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: Const.buttonViewWidthOfFrame),
            buttonView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: Const.buttonViewHeightOfFrame)
        ])
    }

    private func addTargetsForButtons() {
        buttonView.appleMapButton.addTarget(self, action: #selector(openAppleMap), for: .touchUpInside)
        buttonView.googleMapButton.addTarget(self, action: #selector(openGoogleMap), for: .touchUpInside)
        shuffleButton.target = self
        shuffleButton.action = #selector(shuffleStudents)
    }

    @objc private func openAppleMap() {
        appleMapVC = AppleMapViewController(studentModel: studentModel)
        guard let appleVC = appleMapVC else { fatalError("Unknown error occured") }
        navigationController?.pushViewController(appleVC, animated: true)
    }

    @objc private func openGoogleMap() {
        googleMapVC = GoogleMapViewController(studentModel: studentModel)
        guard let googleVC = googleMapVC else { fatalError("Unknown error occured") }
        navigationController?.pushViewController(googleVC, animated: true)
    }

    @objc private func shuffleStudents() {
        studentModel = StudentGroup()
    }
}
