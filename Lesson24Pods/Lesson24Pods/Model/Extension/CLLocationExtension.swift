//
//  CLLocationExtension.swift
//  Lesson24
//
//  Created by Harbros 66 on 1.07.21.
//

import MapKit

extension CLLocation {
    convenience init?(coordinate: CLLocationCoordinate2D?) {
        guard let coordinate = coordinate else { return nil }
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }

    func distance(fromOptional location: CLLocation?) -> CLLocationDistance? {
        guard let location = location else { return nil }
        return self.distance(from: location)
    }
}
