//
//  Student.swift
//  Lesson24
//
//  Created by Harbros 66 on 30.06.21.
//

import MapKit

private enum Const {
    static let maleNames = [
        "james", "robert", "john", "michael", "william", "david", "richard", "joseph", "thomas", "charles"
    ]
    static let femaleNames = [
        "mary", "patricia", "jennifer", "linda", "elizabeth", "barbara", "susan", "jessica", "sarah", "karen"
    ]
    static let lastnames = [
        "smith", "johnson", "williams", "brown", "jones", "miller", "davis", "garcia", "rodriguez", "wilson",
        "martinez", "anderson", "taylor", "thomas", "hernandez", "moore", "martin", "jackson", "thompson", "white"
    ]

    static let femaleImageName = "female"
    static let maleImageName = "male"

    static let maleGoogleImageName = "maleMarker"
    static let femaleGoogleImageName = "femaleMarker"

    static let eldestDate = "01.01.1970"
    static let youngestDate = "01.01.2003"
}

enum Sex: String, CaseIterable {
    case male
    case female

    var randomName: String {
        switch self {
        case .female:
            return Const.femaleNames.randomElement()?.capitalized ?? "Mary"
        case .male:
            return Const.maleNames.randomElement()?.capitalized ?? "James"
        }
    }

    var image: UIImage? {
        switch self {
        case .female:
            return UIImage(named: Const.femaleImageName)
        case .male:
            return UIImage(named: Const.maleImageName)
        }
    }

    var markerImage: UIImage? {
        switch self {
        case .female:
            return UIImage(named: Const.femaleGoogleImageName)
        case .male:
            return UIImage(named: Const.maleGoogleImageName)
        }
    }
}

class Student: NSObject, MKAnnotation {
    let sex: Sex
    let name: String
    let lastname = Const.lastnames.randomElement()?.capitalized ?? "Garcia"
    let birthday = Date.randomBetween(start: Const.eldestDate, end: Const.youngestDate)
    let coordinate: CLLocationCoordinate2D
    var title: String? {
        "\(name) \(lastname)"
    }
    var subtitle: String? {
        birthday.dateString()
    }
    var image: UIImage? {
        sex.image
    }
    var markerImage: UIImage? {
        sex.markerImage
    }

    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        sex = Sex.allCases.randomElement() ?? .male
        name = sex.randomName
    }
}
