//
//  ButtonView.swift
//  Lesson24
//
//  Created by Harbros 66 on 29.06.21.
//

import UIKit

private enum Const {
    static let edgeOffset: CGFloat = 4

    static let buttonHeightOfFrame: CGFloat = 0.5
}

private enum Style {
    static let appleMapButtonTitleText = "Open Apple map"

    static let googleMapButtonTitleText = "Open Google map"

    static let textColor: UIColor = .black
    static let buttonBackgroundColor: UIColor = .systemBlue
    static let buttonBorderWidth: CGFloat = 1
    static let buttonBorderColor: CGColor = UIColor.black.cgColor
    static let buttonBorderCornerRadius: CGFloat = 5
}

class ButtonView: UIView {
    let appleMapButton = UIButton()
    let googleMapButton = UIButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        styleButtons()
        addSubview(appleMapButton)
        addSubview(googleMapButton)
        constraintsForButtons()
    }

    private func constraintsForButtons() {
        appleMapButton.translatesAutoresizingMaskIntoConstraints = false
        googleMapButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            appleMapButton.topAnchor.constraint(equalTo: topAnchor, constant: Const.edgeOffset),
            appleMapButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Const.edgeOffset),
            appleMapButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: Const.edgeOffset),
            appleMapButton.heightAnchor.constraint(
                equalTo: heightAnchor,
                multiplier: Const.buttonHeightOfFrame,
                constant: -Const.edgeOffset
            ),

            googleMapButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Const.edgeOffset),
            googleMapButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Const.edgeOffset),
            googleMapButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: Const.edgeOffset),
            googleMapButton.heightAnchor.constraint(
                equalTo: heightAnchor,
                multiplier: Const.buttonHeightOfFrame,
                constant: -Const.edgeOffset
            )
        ])
    }

    private func styleButtons() {
        appleMapButton.setTitle(Style.appleMapButtonTitleText, for: .normal)
        appleMapButton.setTitleColor(Style.textColor, for: .normal)
        appleMapButton.layer.borderWidth = Style.buttonBorderWidth
        appleMapButton.layer.borderColor = Style.buttonBorderColor
        appleMapButton.layer.cornerRadius = Style.buttonBorderCornerRadius
        appleMapButton.backgroundColor = Style.buttonBackgroundColor

        googleMapButton.setTitle(Style.googleMapButtonTitleText, for: .normal)
        googleMapButton.setTitleColor(Style.textColor, for: .normal)
        googleMapButton.layer.borderWidth = Style.buttonBorderWidth
        googleMapButton.layer.borderColor = Style.buttonBorderColor
        googleMapButton.layer.cornerRadius = Style.buttonBorderCornerRadius
        googleMapButton.backgroundColor = Style.buttonBackgroundColor
    }

    required init?(coder: NSCoder) { super.init(coder: coder) }
}
