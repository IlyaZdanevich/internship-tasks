//
//  WebKitController.swift
//  Lesson25
//
//  Created by Harbros 66 on 3.07.21.
//

import WebKit

private enum Const {
    static let errorMessage = "Error"
}

class WebKitController: UIViewController {
    private let webView = WKWebView(frame: .zero, configuration: .init())
    private let urlLabel = UILabel()
    private let refreshButton = UIBarButtonItem(systemItem: .refresh)

    func load(sourceItem: SourceItem) {
        urlLabel.text = sourceItem.title
        guard let requestURL = sourceItem.itemURL else {
            urlLabel.text = Const.errorMessage
            return
        }
        let request = URLRequest(url: requestURL)
        webView.load(request)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(webView)
        navigationItem.titleView = urlLabel
        setupButton()
        navigationItem.setRightBarButton(refreshButton, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        webView.evaluateJavaScript("document.body.remove()")
        webView.stopLoading()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        webView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    private func setupButton() {
        refreshButton.target = self
        refreshButton.action = #selector(reload)
    }

    @objc private func reload() {
        webView.reload()
    }
}
