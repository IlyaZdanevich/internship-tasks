//
//  ConvenienceFuncs.swift
//  Lesson21
//
//  Created by Harbros 66 on 23.06.21.
//

import UIKit

private enum Const {
    static let carouselFrameHeightToMainScreen: CGFloat = 0.2
    static let adFrameHeightToMainScreen: CGFloat = 0.15
    static var mainCatalogueHeightToMainScreen: CGFloat {
        1 - carouselFrameHeightToMainScreen - adFrameHeightToMainScreen
    }

    static let preferredMainCatalogueWidth: CGFloat = 100
    static let preferredMainCatalogueHeight: CGFloat = 110

    static let topEdgeInset: CGFloat = 2
    static let leftEdgeInset: CGFloat = 2
    static let bottomEdgeInset: CGFloat = 2
    static let rightEdgeInset: CGFloat = 2
    static var widthSizeOffset: CGFloat {
        leftEdgeInset + rightEdgeInset
    }
    static var heightSizeOffset: CGFloat {
        topEdgeInset + bottomEdgeInset
    }

    static let adViewItemDivisionRate: CGFloat = 0.5
}

class ConvenienceFuncs {
    // CarouselLayout
    static func carouselLayout(_ carouselFrame: CGRect) -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()

        layout.sectionInset = UIEdgeInsets(
            top: Const.topEdgeInset,
            left: Const.leftEdgeInset,
            bottom: Const.bottomEdgeInset,
            right: Const.rightEdgeInset
        )
        layout.itemSize = CGSize(
            width: carouselFrame.width - Const.widthSizeOffset,
            height: carouselFrame.height - Const.heightSizeOffset
        )
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = Const.widthSizeOffset

        return layout
    }

    static func mainCatalogueLayout(_ mainCatalogueFrame: CGRect) -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()

        layout.sectionInset = .zero
        layout.itemSize = CGSize(
            width: mainCatalogueFrame.width /
                floor(mainCatalogueFrame.width / Const.preferredMainCatalogueWidth),
            height: mainCatalogueFrame.height /
                floor(mainCatalogueFrame.height / Const.preferredMainCatalogueHeight)
        )
        layout.minimumLineSpacing = .zero
        layout.minimumInteritemSpacing = .zero
        layout.scrollDirection = .vertical

        return layout
    }

    static func adLayout(_ adFrame: CGRect) -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()

        layout.sectionInset = UIEdgeInsets(
            top: Const.topEdgeInset,
            left: Const.leftEdgeInset,
            bottom: Const.bottomEdgeInset,
            right: Const.rightEdgeInset
        )
        layout.itemSize = CGSize(
            width: (adFrame.width * Const.adViewItemDivisionRate) - Const.widthSizeOffset,
            height: adFrame.height - Const.heightSizeOffset
        )
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = Const.widthSizeOffset

        return layout
    }
}
