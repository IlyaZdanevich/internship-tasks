//
//  Strings.swift
//  lesson7
//
//  Created by Harbros 66 on 18.05.21.
//

import Foundation

class StringsLesson {
    func taskOne(stringArray: [String]) -> String {
        let stringsToUpper = stringArray.compactMap { $0.capitalized }.joined(separator: " ")
        return stringsToUpper
    }
    func taskTwo(inputString: String) -> [String] {
        let wordArray = inputString.split(separator: " ").map { String($0) }
        return wordArray
    }
    func taskThree(stringArray: [String]) -> String {
        guard let longestString = stringArray.max(by: { $1.count > $0.count }) else { return "Your input is wrong" }
        return longestString
    }
    func taskFour(inputString: String) -> String {
        let uniqueInReverse = inputString.reduce("") { uniqueChars, nextChar in
            uniqueChars.contains(nextChar) ? uniqueChars : uniqueChars + String(nextChar)
        }
        .reversed()
        .reduce("") { $0 + String($1) }
        return uniqueInReverse
    }
    func taskFive(inputString: String) -> [String] {
        let intArray = inputString.map { $0.isNumber ? $0 : " " }
            .reduce("", { $0 + String($1) })
            .split(separator: " ")
            .map { String($0) }
        return intArray
    }
}
